#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

class Level0 {
public:
  Level0();
  void solve();

  // Mutators
  string get_content() { return content; }
  string get_dict() { return dict_path; }

  // Accessors
  void set_content(string& c) { content = c; }
  void set_dict(string dict) { dict_path = dict; }

private:
  string content;
  vector<string> words;
  string dict_path;
};

Level0::Level0() {
  dict_path = "/usr/share/dict/words";
}

void Level0::solve() {
  fstream file;
  words.resize(25000);
  file.open(dict_path.c_str(), std::fstream::in);
  int w = 0;
  if(file.is_open()) {
    string l;
    while(!file.eof()) {
      getline(file, l);
      words.push_back(l);
    }
  } else {
    cerr << "Could not find file specified: " << dict_path << endl;
  }
  file.close();
}

int main(int argc, char* argv[]) {
  Level0 level;

  string content;
  cin >> content;
  level.set_content(content);

  if(argc > 1) level.set_dict(argv[1]);

  level.solve();

}